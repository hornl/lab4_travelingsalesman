package edu.msoe.se2832.travelingsalesman;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * This class fully tests the MSOECommissionCalculator class by using a series of NGTest suites
 * 
 * @author hornl, duchesneaur, heinzer
 *
 */
public class CommisionCalculatorTester {
	/**
	 * Notify test starting 
	 */
	@BeforeClass
	public void beforeClass(){
		System.out.println("Starting to test the commission calculator");
	}
	/**
	 * Notify test ending
	 */
	@AfterClass
	public void afterClass(){
		System.out.println("The Testing is finished. ");
	}
	
	/**
	 * This is an instance of the calculator to use for testing. 
	 */
	private MSOECommissionCalculator salesman;
	
//**************************** Test Constructor Suite*****************************
	@Test
	/**
	 * Test to make sure a new salesman is made correctly. 
	 * @author hornl
	 */
	public void testValidProbationaryConstructor()throws Exception{
		salesman = new MSOECommissionCalculator("James Barkly", 0);
		assert salesman != null;
		assert salesman.getName().equals("James Barkly");
		assert salesman.getEmployeeExperience() == 0;
		
		salesman = new MSOECommissionCalculator("James Barkly", iCommissionCalculator.PROBATIONARY);
		assert salesman != null;
		assert salesman.getName().equals("James Barkly");
		assert salesman.getEmployeeExperience() == 0;
	}
	
	@Test
	/**
	 * Test to make sure an experienced salesman is made correctly. 
	 * @author hornl
	 */
	public void testValidExperiencedConstructor()throws Exception{
		salesman = new MSOECommissionCalculator("James Barkly", 1);
		assert salesman != null;
		//assert salesman.getName().equals("James Barkly");
		assert salesman.getEmployeeExperience() == 1;
		//TODO: error: Only the first name of the employee is saved and returned. 
		
		salesman = new MSOECommissionCalculator("James Barkly", iCommissionCalculator.EXPERIENCED);
		assert salesman != null;
		//assert salesman.getName().equals("James Barkly");
		assert salesman.getEmployeeExperience() == 1;
	}
	
	/**
	 * This data provider contains arguments that are all invalid and should 
	 * all throw exceptions when they are passed into the constructor. 
	 * @return
	 * @author hornl
	 */
	@DataProvider(name= "invalidConstructorTests")
	public static Object[][] createInvalidConstructorData(){
		return new Object [][]{
				{"James Barkly", new Integer(-1)},
				{"James Barkly", new Integer(2)},
				{"James", new Integer(0)},
				{"J", new Integer(0)},
				{"", new Integer(0)},
				{null, new Integer(0)}
		};
	}
	
	//TODO: Maybe check for separate error messages. 
	/**
	 * Actual system to test the invalid inputs, uses the above data provider. 
	 * @author hornl
	 * @param name The name of the salesman
	 * @param age  The age of the salesman
	 * @throws Exception An exception should be thrown everytime. 
	 * 
	 */
	@Test(dataProvider = "invalidConstructorTests", expectedExceptions=Exception.class)
	public void testInvalidConstructor(String name, int age) throws Exception{
		salesman = new MSOECommissionCalculator(name, age);
	}
	
//**********************************Add sale Test suite*******************************
	/*
	 * @author hornl
	 */
	
	/**
	 * Tests adding all four basic sales to the account of a probationary worker. 
	 * @author hornl
	 * @throws Exception 
	 */
	@Test
	public void testAddSaleProbationary() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 0);
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 200);
		assert salesman.getTotalSales()==200;
		salesman.addSale(iCommissionCalculator.CONSULTING_ITEM, 0);
		assert salesman.getTotalSales()==200;
		salesman.addSale(iCommissionCalculator.MAINTENANCE_ITEM, 100000.00);
		assert salesman.getTotalSales()==100200;
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 200);
		assert salesman.getTotalSales()==100400;
	}
	
	/**
	 * Tests adding all four basic sales to the account of an experienced worker. 
	 * @author hornl
	 * @throws Exception
	 */
	@Test
	public void testAddSaleExperienced() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 1);
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 0);
		assert salesman.getTotalSales()==0;
		salesman.addSale(iCommissionCalculator.CONSULTING_ITEM, 200);
		assert salesman.getTotalSales()==200;
		salesman.addSale(iCommissionCalculator.MAINTENANCE_ITEM, 200);
		assert salesman.getTotalSales()==400;
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 100000.00);
		assert salesman.getTotalSales()==100400;
	}

	/**
	 * Creates arrays of invalid input for the addSale method, including 
	 * both incorrect sales types and incorrect dollar amounts. 
	 * @author hornl
	 * @return
	 */
	@DataProvider(name= "invalidSalesTests")
	public static Object[][] createInvalidSalesData(){
		return new Object [][]{
				{-1, 200},
				{4, 200},
				{iCommissionCalculator.BASIC_ITEM, -1},
				{iCommissionCalculator.BASIC_ITEM, 100000.01},
				{iCommissionCalculator.BASIC_ITEM, 100001.00},
		};
	}
	
	/**
	 * Tests the invalid input types for the addSales method. 
	 * @author hornl
	 * @param salesType The type of sale that occurred
	 * @param dollarAmount The amount the sale was for. 
	 * @throws Exception Should always be thrown. 
	 */
	@Test(dataProvider = "invalidSalesTests", expectedExceptions=Exception.class)
	public void testInvalidSalesData(int salesType, double dollarAmount) throws Exception{
		salesman.addSale(salesType, dollarAmount);
	}
	
	
//**********************************getTotalSales Test suite***************************

	/**
	 * Tests that the getTotalSales() method is working as intended
	 * and returns the total amount of sales a salesman earns
	 *
	 * @throws Exception
	 * @author heinzer
	 */
	@Test
	public void testGetTotalSales() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 0);
		assert salesman.getTotalSales()==0;
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 200);
		assert salesman.getTotalSales()== 200;
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 400);
		assert salesman.getTotalSales()== 600;
	}
	
	
//***********************************Set Experience Test suite*************************
	
	/**
	 * Tests the set experience method with an integer
	 * that is not equal to either statuses of experienced
	 * or probationary.
	 * 
	 * @throws Exception Should be thrown
	 * @author heinzer
	 */
	@Test(expectedExceptions = Exception.class)
	public void testSetExperience_InvalidExperience() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 1);
		salesman.setEmployeeExperience(3);
	}
	
	/**
	 * Tests the set experience method with an integer equal
	 * to that of a probationary status
	 * @throws Exception Should not be thrown
	 * @author heinzer
	 */
	@Test
	public void testSetExperience_Probationary() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 1);
		salesman.setEmployeeExperience(0);
		assert salesman.getEmployeeExperience() == 0;
	}
	
	/**
	 * Tests the set experience method with an integer equal
	 * to that of a experienced status
	 * @throws Exception Should not be thrown
	 * @author heinzer
	 */
	@Test
	public void testSetExperience_Experienced() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 0);
		salesman.setEmployeeExperience(1);
		assert salesman.getEmployeeExperience() == 1;
	}
	
//***********************************Calculate Commission Test suite*******************
	/**
	 * Tests the commissions for a probationary salesman under the minimum limit
	 * @author hornl
	 * @throws Exception
	 */
	@Test
	public void testCommissionProbationaryUnder() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 0);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 100.00);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 250.00);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 200.00);
		salesman.addSale(iCommissionCalculator.CONSULTING_ITEM, 1000.00);
		salesman.addSale(iCommissionCalculator.MAINTENANCE_ITEM, 250.00);
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 200);
		
		assert salesman.calculateCommission()==0;
	}
	
	/**
	 * Tests the commissions for a probationary salesman over the minimum limit 
	 * @author hornl
	 * @throws Exception
	 */
	@Test
	public void testCommissionProbationaryOver() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 0);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 100.00);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 250.00);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 200.00);
		salesman.addSale(iCommissionCalculator.CONSULTING_ITEM, 1000.00);
		salesman.addSale(iCommissionCalculator.MAINTENANCE_ITEM, 250.00);
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 200);
		salesman.addSale(iCommissionCalculator.MAINTENANCE_ITEM, 400);
		assert salesman.calculateCommission()==12;
	}
	
	/**
	 * Tests the commission for an experienced salesman under the minimum limit
	 * @author hornl
	 * @throws Exception
	 */
	@Test
	public void testCommissionExperiencedUnder() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 1);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 1500.00);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 2500.00);
		assert salesman.calculateCommission()==0;
	}
	
	/**
	 * Tests the commission for an experienced salesman over the minimum limit
	 * @author hornl
	 * @throws Exception
	 */
	@Test
	public void testCommissionExperiencedOver() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 1);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 1500.00);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 2500.00);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 2000.00);
		salesman.addSale(iCommissionCalculator.CONSULTING_ITEM, 10000.00);
		salesman.addSale(iCommissionCalculator.MAINTENANCE_ITEM, 25000.00);
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 9000);
		salesman.addSale(iCommissionCalculator.MAINTENANCE_ITEM, 40000);
		salesman.addSale(iCommissionCalculator.REPLACEMNET_ITEM, 10000);
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 1000);
		salesman.addSale(iCommissionCalculator.CONSULTING_ITEM, 5000);
		assert salesman.calculateCommission()==5665;
	}
	
	/**
	 * Data used to test the probationary commission percentages of different types of items separately. 
	 * @author hornl
	 * @return
	 */
	@DataProvider(name= "commissionProbationTests")
	public static Object[][] createCommissionPTestsData(){
		return new Object [][]{
				{iCommissionCalculator.BASIC_ITEM, 100, 2},
				{iCommissionCalculator.MAINTENANCE_ITEM, 100, 3},
				{iCommissionCalculator.REPLACEMNET_ITEM, 100, 1},
				{iCommissionCalculator.CONSULTING_ITEM, 100, 3},
		};
	}
	
	/**
	 * Tests out the different commission percentages for a probationary salesman one by one 
	 * @author hornl
	 * @param type the type of sale
	 * @param amount the amount of the sale
	 * @param expected The expected commission. 
	 * @throws Exception
	 */
	@Test(dataProvider="commissionProbationTests")
	public void testCommissionProbationaryPercentages(int type, double amount, double expected) throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 0);
		// bring the sale up to the minimum required for bonus. 
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 2000.00);
		salesman.addSale(type, amount);
		assert salesman.calculateCommission()==expected;
	}
	
	
	/**
	 * Data used to test the experienced commission percentages of different types of items separately. 
	 * @author hornl
	 * @return
	 */
	@DataProvider(name= "commissionExperienceTests")
	public static Object[][] createCommissionETestsData(){
		return new Object [][]{
				{iCommissionCalculator.BASIC_ITEM, 100, 4},
				{iCommissionCalculator.MAINTENANCE_ITEM, 100, 6},
				{iCommissionCalculator.REPLACEMNET_ITEM, 100, 1.50},
				{iCommissionCalculator.CONSULTING_ITEM, 100, 8},
		};
	}
	
	/**
	 * Tests out the different commission percentages for an experienced salesman one by one 
	 * @author hornl
	 * @param type the type of sale
	 * @param amount the amount of the sale
	 * @param expected The expected commission. 
	 * @throws Exception
	 */
	@Test(dataProvider="commissionExperienceTests")
	public void testCommissionExperiencedPercentages(int type, double amount, double expected) throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 1);
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 5000.00);
		salesman.addSale(type, amount);
		assert salesman.calculateCommission()==expected;
	}
	
	/**
	 * tests the calculateCommission method when there are no recorded sales.
	 * @author hornl
	 * @throws Exception
	 */
	@Test
	public void testCommissionNoSales() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 1);
		assert salesman.calculateCommission()==0;
	}
	
	
//*************************************Calculate Bonus Commission Test suite***********
	
	/***
	 * Tests the calculateBonusCommission() method with an experienced salesman
	 * at the boundary points in the method.
	 * @throws Exception Should not throw exception
	 * @author heinzer
	 */
	@Test
	public void testCalculateBonusCommission_Experienced() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 1);
		
		//netSales < bonus commission 
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 99999.0);
		Assert.assertEquals(salesman.calculateBonusCommission(), (0.015 * (0)), 0.1);
		
		//netSales = bonus comission
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 1.0);
		Assert.assertEquals(salesman.calculateBonusCommission(), (0.015 * (99999.0 + 1.0 - 100000.0)), 0.1);
				
		//netSales > bonus commission
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 1.0);
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 1.0);
		Assert.assertEquals(salesman.calculateBonusCommission(), (0.015 * (99999.0 + 1.0 + 1.0 + 1.0 - 100000.0)), 0.01);
	}
	
	/***
	 * Tests the calculateBonusCommission() method with a probationary salesman
	 * at the boundary points in the method.
	 * @throws Exception Should not throw exception
	 * @author heinzer
	 */
	@Test
	public void testCalculateBonusCommission_Probationary() throws Exception{
		salesman = new MSOECommissionCalculator("James Berkley", 0);
		//minimum probationary sales for bonus commission = 50,000
		
		//netSales < bonus commission 
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 49999.0);
		Assert.assertEquals(salesman.calculateBonusCommission(), (0.005 * (0.0)), 0.1);
		
		//netSales = bonus comission
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 1.0);
		Assert.assertEquals(salesman.calculateBonusCommission(), (0.005 * (49999.0 + 1.0 - 50000.0) ), 0.1);
		
		//netSales > bonus commission
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 1.0);
		salesman.addSale(iCommissionCalculator.BASIC_ITEM, 1.0);
		Assert.assertEquals(salesman.calculateBonusCommission(), (0.005 * (49999.0 + 1.0 + 1.0 + 1.0 - 50000.0)), 0.01);
	
	}
	
	
//************************************** getMinimumSales Test suite********************
	
	@Test
	/**
	 * @author duchesneaur
	 * Tests the get method for the value of the minimum sales for a salesman 
	 * of the probationary status 
	 * @throws Exception - Exception should not be thrown
	 */
	public void testGetMinimumSalesProbationary() throws Exception{
		salesman = new MSOECommissionCalculator("Lisa Perkley", iCommissionCalculator.PROBATIONARY);
		assert salesman.getMinimumSales() == 2000;
	}
	
	@Test
	/**
	 * @author duchesneaur
	 * Tests the get method for the value of the minimum sales for a salesman 
	 * of the experienced status 
	 * @throws Exception - Exception should not be thrown
	 */
	public void testGetMinimumSalesExperienced() throws Exception{
		salesman = new MSOECommissionCalculator("Lisa Perkley", iCommissionCalculator.EXPERIENCED);
		assert salesman.getMinimumSales() == 5000;
	}
	
	
	@Test
	/**
	 * @author duchesneaur
	 * Tests the get method for the value of the minimum sales for a salesman 
	 * of both probationary and experienced statuses, but using the interger
	 * values directly instead of the given values
	 * @throws Exception - should not be thrown
	 */
	public void testGetMinimumSalesHardcodeNumber() throws Exception{
		salesman = new MSOECommissionCalculator("Lisa Perkley", 0);
		assert salesman.getMinimumSales() == 2000;
		
		salesman = new MSOECommissionCalculator("Lisa Perkley", 1);
		assert salesman.getMinimumSales() == 5000;
	}
	
	
//***************************************get Name Test suite****************************
	
	@Test
	/**
	 * @author duchesneaur
	 * Test to check that names can be retrieved using the getName method.
	 * Check to make sure that experience of the salesperson does not affect
	 * the name retrieval in any way.
	 */
	public void testGetName()throws Exception{
		salesman = new MSOECommissionCalculator("Timothy Bond", 0);
		assert salesman.getName().equals("Timothy Bond");

		salesman = new MSOECommissionCalculator("Lisa Perkley", iCommissionCalculator.PROBATIONARY);
		assert salesman.getName().equals("Lisa Perkley");
		
		salesman = new MSOECommissionCalculator("Sophie Fames", iCommissionCalculator.EXPERIENCED);
		assert salesman.getName().equals("Sophie Fames");
	}
	
	
//***************************************get Employee Experience Test suite**************
	
	@Test
	/**
	 * @author duchesneaur
	 * Test to check that employee experience is properly assigned and can be retrieved
	 * using getEmployeeExperience for a probationary -> experienced -> probationary salesperson. 
	 * Also double-checks that experience can be re-assigned with
	 * setEmployeeExperience (primary tests are in the testSetEmployeeExperience)
	 * and that the changed value carries through.
	 * 
	 * @throws Exception - should not be thrown
	 */
	public void testGetEmployeeExperienceProbationary()throws Exception{

		salesman = new MSOECommissionCalculator("Lisa Perkley", iCommissionCalculator.PROBATIONARY);
		assert salesman.getEmployeeExperience() == iCommissionCalculator.PROBATIONARY;
		salesman.setEmployeeExperience(iCommissionCalculator.EXPERIENCED);
		assert salesman.getEmployeeExperience() == iCommissionCalculator.EXPERIENCED;
		salesman.setEmployeeExperience(iCommissionCalculator.PROBATIONARY);
		assert salesman.getEmployeeExperience() == iCommissionCalculator.PROBATIONARY;
	
	}
	
	@Test
	/**
	 * @author duchesneaur
	 * Test to check that employee experience is properly assigned and can be retrieved
	 * using getEmployeeExperience for an experienced -> probationary -> experienced salesperson. 
	 * Also double-checks that experience can be re-assigned with
	 * setEmployeeExperience (primary tests are in the testSetEmployeeExperience)
	 * and that the changed value carries through.
	 * 
	 * @throws Exception - should not be thrown
	 */
	public void testGetEmployeeExperienceExperienced()throws Exception{
				
		salesman = new MSOECommissionCalculator("Lisa Perkley", iCommissionCalculator.EXPERIENCED);
		assert salesman.getEmployeeExperience() == iCommissionCalculator.EXPERIENCED;
		salesman.setEmployeeExperience(iCommissionCalculator.PROBATIONARY);
		assert salesman.getEmployeeExperience() == iCommissionCalculator.PROBATIONARY;
		salesman.setEmployeeExperience(iCommissionCalculator.EXPERIENCED);
		assert salesman.getEmployeeExperience() == iCommissionCalculator.EXPERIENCED;
	}
	
}
